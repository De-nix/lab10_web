import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClientsComponent} from './clients/clients.component';
import {BooksComponent} from './books/books.component';
import {PurchasesComponent} from './purchases/purchases.component';
import {BookListComponent} from './books/book-list/book-list.component';
import {BookNewComponent} from './books/book-new/book-new.component';


const routes: Routes = [
  // {path:'books' component:}
  {path: 'clients', component: ClientsComponent},
  {path: 'books', component: BooksComponent},
  {path: 'purchases', component: PurchasesComponent},
  {path: 'book-list', component: BookListComponent},
  {path: 'book-new', component: BookNewComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
